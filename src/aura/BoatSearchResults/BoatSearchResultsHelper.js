({
	
	onSearch : function(component,boatTypeId) {
		// Request from server
		
		console.info("boatTypeId in BoatSearchResultHelper.onSearch: " + boatTypeId);
		var action = component.get("c.getBoats");
		// assign empty boatTypeId
					
			action.setParams({"boatTypeId":boatTypeId});		
		 
	//	debugger;
		action.setCallback(this, function(result){
            
            var status = result.getState();
          //  alert(status);
            if(status === "SUCCESS"){
               
             	
                     //console.log('data '+ JSON.parse(result.getReturnValue()));
                    var boats = result.getReturnValue();    
                    console.log(boats);        		
                    component.set("v.boats",boats);
                    var b = component.get('v.boats');
				
                }
                else{
                    console.log('error');
                }
			
			
        });
        $A.enqueueAction(action);
	}

})