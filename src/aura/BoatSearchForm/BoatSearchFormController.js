({
    init : function(component, event, helper) {
        
        var action= component.get("c.getAllboattypes");
        var createRecordEvent = $A.get("e.force:createRecord");

       // alert(createRecordEvent);
        if(createRecordEvent == undefined)
        {
           component.set("v.hidenewbutton",false); 
        }
        else
        {
            component.set("v.hidenewbutton",true);
        }

        action.setCallback(this,function(response){
            
            var state = response.getState();
            if(state === 'SUCCESS')            
           {   
                component.set("v.boattypes", response.getReturnValue() );
                
            }
        });
       
        $A.enqueueAction(action);
   
    },
    onselect : function(component, event, helper) {
        
       var boatid = component.find('select').get('v.value');
       component.set('v.selected',boatid);
     //  alert(component.get('v.selected'));
    },

    Newrecord: function(component,event,helper){
        var createRecordEvent = $A.get("e.force:createRecord");
       
        var boattype= component.get('v.selected');
        createRecordEvent.setParams({
            "entityApiName": "Boat__c",
            'defaultFieldValues' : {
            "BoatType__c": boattype
            }
        });
        createRecordEvent.fire();
    },
     onFormSubmit: function(component,event,helper)
     {
        var boatTypeId = component.get("v.selected");
       
        var formSubmit = component.getEvent("formsubmit");
        formSubmit.setParams({"formData":
                            {"boatTypeId" : boatTypeId}
        });
        formSubmit.fire();
     }


})